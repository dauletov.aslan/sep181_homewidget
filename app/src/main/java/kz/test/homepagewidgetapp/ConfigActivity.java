package kz.test.homepagewidgetapp;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ConfigActivity extends AppCompatActivity {

    private int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Hello", "onCreate config");
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        Intent resultIntent = new Intent();
        resultIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        setResult(RESULT_CANCELED, resultIntent);

        setContentView(R.layout.config);

        Button okButton = findViewById(R.id.okButton);
        RadioGroup radioGroup = findViewById(R.id.colorRadioGroup);
        EditText editText = findViewById(R.id.textEditText);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioButtonId = radioGroup.getCheckedRadioButtonId();
                int color = Color.RED;
                switch (radioButtonId) {
                    case R.id.redRadioButton:
                        color = Color.RED;
                        break;
                    case R.id.blueRadioButton:
                        color = Color.BLUE;
                        break;
                    case R.id.greenRadioButton:
                        color = Color.GREEN;
                        break;
                }

                SharedPreferences sharedPreferences = getSharedPreferences("widget", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("widget_text" + widgetId, editText.getText().toString());
                editor.putInt("widget_color" + widgetId, color);
                editor.commit();

                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(ConfigActivity.this);
                MyWidget.updateWidget(ConfigActivity.this, appWidgetManager, sharedPreferences, widgetId);

                setResult(RESULT_OK, resultIntent);
                Log.d("Hello", "Finish config: " + widgetId);
                finish();
            }
        });
    }
}
