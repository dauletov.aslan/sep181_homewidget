package kz.test.homepagewidgetapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ActionWidget extends AppWidgetProvider {

    private final static String action = "kz.test.homepagewidgetapp.UPDATE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        for (int id : appWidgetIds) {
            updateWidget(context, appWidgetManager, id);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        SharedPreferences.Editor editor = context.getSharedPreferences("action_widget", Context.MODE_PRIVATE).edit();
        for (int widgetId : appWidgetIds) {
            editor.remove("widget_time_format" + widgetId);
            editor.remove("widget_count" + widgetId);
        }
        editor.commit();
    }

    static void updateWidget(Context context, AppWidgetManager appWidgetManager, int widgetId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("action_widget", Context.MODE_PRIVATE);

        String timeFormat = sharedPreferences.getString("widget_time_format" + widgetId, "");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(timeFormat);
        String currentTime = simpleDateFormat.format(new Date(System.currentTimeMillis()));

        String count = sharedPreferences.getInt("widget_count" + widgetId, 0) + "";

        RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.action_widget_layout);
        widgetView.setTextViewText(R.id.updateTimeTextView, currentTime);
        widgetView.setTextViewText(R.id.countTextView, count);

        Intent configIntent = new Intent(context, ActionConfigActivity.class);
        configIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
        configIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, widgetId, configIntent, 0);
        widgetView.setOnClickPendingIntent(R.id.configButton, pendingIntent);

        Intent updateIntent = new Intent(context, ActionWidget.class);
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, new int[]{widgetId});
        pendingIntent = PendingIntent.getBroadcast(context, widgetId, updateIntent, 0);
        widgetView.setOnClickPendingIntent(R.id.updateButton, pendingIntent);

        Intent countIntent = new Intent(context, MyWidget.class);
        countIntent.setAction(action);
        configIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        pendingIntent = PendingIntent.getBroadcast(context, widgetId, countIntent, 0);
        widgetView.setOnClickPendingIntent(R.id.countButton, pendingIntent);

        appWidgetManager.updateAppWidget(widgetId, widgetView);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equalsIgnoreCase(action)) {
            int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
            Bundle extras = intent.getExtras();
            if (extras != null) {
                appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            }
            if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("action_widget", Context.MODE_PRIVATE);
                int count = sharedPreferences.getInt("widget_count" + appWidgetId, 0);
                sharedPreferences.edit().putInt("widget_count" + appWidgetId, ++count).commit();

                updateWidget(context, AppWidgetManager.getInstance(context), appWidgetId);
            }
        }
    }
}
