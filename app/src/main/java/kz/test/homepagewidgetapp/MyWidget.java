package kz.test.homepagewidgetapp;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Arrays;
import java.util.Random;

public class MyWidget extends AppWidgetProvider {

    private static String[] texts = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth"};

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d("Hello", "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.d("Hello", "onUpdate: " + Arrays.toString(appWidgetIds));

        SharedPreferences sharedPreferences = context.getSharedPreferences("widget", Context.MODE_PRIVATE);
        for (int id : appWidgetIds) {
            updateWidget(context, appWidgetManager, sharedPreferences, id);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d("Hello", "onDeleted: " + Arrays.toString(appWidgetIds));

        SharedPreferences sharedPreferences = context.getSharedPreferences("widget", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (int id : appWidgetIds) {
            editor.remove("widget_text" + id);
            editor.remove("widget_color" + id);
        }
        editor.commit();
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d("Hello", "onDisabled");
    }

    static void updateWidget(Context context, AppWidgetManager appWidgetManager, SharedPreferences sharedPreferences, int id) {
        Log.d("Hello", "updateWidget: " + id);
        String text = sharedPreferences.getString("widget_text" + id, "");
        int color = sharedPreferences.getInt("widget_color" + id, Color.RED);

        RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        int randomId = new Random().nextInt(6);
        widgetView.setTextViewText(R.id.textView, texts[randomId]);
        widgetView.setInt(R.id.textView, "setBackgroundColor", color);

        appWidgetManager.updateAppWidget(id, widgetView);
    }
}
