package kz.test.homepagewidgetapp;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActionConfigActivity extends AppCompatActivity {

    private int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    private SharedPreferences sharedPreferences;
    private EditText editText;
    private Intent resultIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        resultIntent = new Intent();
        resultIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        setResult(RESULT_CANCELED, resultIntent);

        setContentView(R.layout.action_config);

        sharedPreferences = getSharedPreferences("action_widget", MODE_PRIVATE);
        editText = findViewById(R.id.formatEditText);
        editText.setText(sharedPreferences.getString("widget_time_format" + widgetId, "HH:mm:ss"));

        int count = sharedPreferences.getInt("widget_count" + widgetId, -1);
        if (count == -1) {
            sharedPreferences.edit().putInt("widget_count" + widgetId, 0);
        }
    }

    public void onClick(View view) {
        sharedPreferences.edit().putString("widget_time_format" + widgetId, editText.getText().toString()).commit();
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
